from time import sleep

from pages.base import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from tests.data import TestData
from selenium.webdriver.common.keys import Keys


class BoardPage(BasePage):
    title = "{0} | Trello".format(TestData.test_board_name)
    error_title = "Błąd | Trello"
    menu_more_button = (By.PARTIAL_LINK_TEXT, "Więcej")
    menu_more_back_button = (By.CLASS_NAME, "board-menu-header-back-button")
    menu_close_board_button = (By.PARTIAL_LINK_TEXT, "Zamknij tablicę")
    menu_close_board_confirm_button = (By.CLASS_NAME, "js-confirm")
    reopen_board_button = (By.CLASS_NAME, "js-reopen")
    permanently_delete_board_link = (By.PARTIAL_LINK_TEXT, "Trwale usuń tablicę")
    menu_delete_board_confirm_button = (By.CLASS_NAME, "js-confirm")
    board_not_found_message_selector = (By.TAG_NAME, "h1")
    board_not_found_message_text = "Tablicy nie znaleziono."

    rename_board_button = (By.CLASS_NAME, "js-rename-board")
    rename_board_dialog_name_field = (By.CLASS_NAME, "js-board-name")
    rename_board_dialog_rename_button = (By.CSS_SELECTOR, "input.primary.wide.js-rename-board")

    # board_permission_level = (By.CSS_SELECTOR, "board-header-btn-icon.icon-sm icon-public")
    board_permission_level_button = (By.ID, "permission-level")
    board_current_permission_level_icon = (By.CSS_SELECTOR, "a#permission-level>span")
    board_permission_level_private = (By.NAME, "private")
    board_permission_level_public = (By.NAME, "public")
    board_menu_default_view = (By.CLASS_NAME, "is-board-menu-default-view")

    menu_copy_board_button = (By.CLASS_NAME, "js-copy-board")
    menu_copy_board_new_title = (By.ID, "boardNewTitle")
    menu_copy_board_create_button = (By.CSS_SELECTOR, "input.primary.wide.js-submit")

    save_new_list_button = (By.CLASS_NAME, "mod-list-add-button")
    name_of_new_list_field = (By.CLASS_NAME, "list-name-input")

    name_of_added_list = (By.CLASS_NAME, "js-list-name-input")
    change_name_field = (By.CLASS_NAME, "js-editing-target")

    list_menu_button = (By.CLASS_NAME, "list-header-extras-menu")
    list_menu_copy_list_option = (By.CLASS_NAME, "js-copy-list")
    list_menu_new_name_field = (By.CLASS_NAME, "js-autofocus")
    list_menu_copy_list_submit_button = (By.CLASS_NAME, "js-submit")
    list_menu_archive_list_option = (By.LINK_TEXT, "Zarchiwizuj tę listę")

    def __send_keys_letter_by_letter(self, element, text):
        text_field = self.driver.find_element(*element)
        text_field.click()
        text_field.clear()
        for letter in text:
            text_field.send_keys(letter)

    def delete_default_lists_if_such_exist(self):
        set_of_new_lists = self.driver.find_elements(*BoardPage.name_of_added_list)
        if len(set_of_new_lists) != 0:
            BoardPage.archive_all_lists(self)

    def rename_board(self, new_board_name):
        self.driver.find_element(*BoardPage.rename_board_button).click()
        self.__send_keys_letter_by_letter(BoardPage.rename_board_dialog_name_field, new_board_name)
        self.driver.find_element(*BoardPage.rename_board_dialog_rename_button).click()
        if new_board_name:
            self.wait.until(expected_conditions.invisibility_of_element_located(BoardPage.rename_board_dialog_rename_button))

    def check_if_rename_button_is_visible(self):
        return self.driver.find_element(*BoardPage.rename_board_dialog_rename_button).is_displayed()

    def change_board_permission_level_to_private(self):
        self.driver.find_element(*BoardPage.board_permission_level_button).click()
        self.driver.find_element(*BoardPage.board_permission_level_private).click()

    def change_board_permission_level_to_public(self):
        self.driver.find_element(*BoardPage.board_permission_level_button).click()
        self.driver.find_element(*BoardPage.board_permission_level_public).click()

    def copy_board(self, copy_board_name):
        self.wait.until(expected_conditions.element_to_be_clickable(BoardPage.menu_more_button))
        self.driver.find_element(*BoardPage.menu_more_button).click()
        self.wait.until(expected_conditions.element_to_be_clickable(BoardPage.menu_copy_board_button))
        self.driver.find_element(*BoardPage.menu_copy_board_button).click()
        self.__send_keys_letter_by_letter(BoardPage.menu_copy_board_new_title, copy_board_name)
        self.driver.find_element(*BoardPage.menu_copy_board_create_button).click()
        self.wait.until(expected_conditions.text_to_be_present_in_element(BoardPage.rename_board_button, copy_board_name))
        # self.wait.until(expected_conditions.invisibility_of_element_located(BoardPage.menu_copy_board_button))

    def __get_board_permission_level(self):
        attribute = self.driver.find_element(*BoardPage.board_current_permission_level_icon).get_attribute("class")
        return attribute

    def is_current_board_private(self):
        return self.__get_board_permission_level().find("icon-private")

    def is_current_board_public(self):
        return self.__get_board_permission_level().find("icon-public")

    def is_board_created_correctly(self, board_name):
        return self.driver.find_element(*BoardPage.rename_board_button).text == board_name

    def close_current_board(self):
        self.wait.until(expected_conditions.presence_of_element_located(BoardPage.menu_more_button))
        self.driver.find_element(*BoardPage.menu_more_button).click()
        self.wait.until(expected_conditions.presence_of_element_located(BoardPage.menu_close_board_button))
        self.driver.find_element(*BoardPage.menu_close_board_button).click()
        self.driver.find_element(*BoardPage.menu_close_board_confirm_button).click()

    def reopen_current_board(self):
        self.driver.find_element(*BoardPage.reopen_board_button).click()
        # self.wait.until(expected_conditions.element_to_be_clickable)

    def click_board_menu_back_button(self):
        self.driver.find_element(*BoardPage.menu_more_back_button).click()
        sleep(1)
        # self.wait.until(expected_conditions.visibility_of_element_located(BoardPage.board_menu_default_view))

    def delete_current_board(self):
        self.close_current_board()
        self.driver.find_element(*BoardPage.permanently_delete_board_link).click()
        self.driver.find_element(*BoardPage.menu_delete_board_confirm_button).click()

    def is_board_deleted_correctly(self):
        self.wait.until(expected_conditions.title_contains(BoardPage.error_title))
        return self.driver.find_element(
            *BoardPage.board_not_found_message_selector).text == BoardPage.board_not_found_message_text

    def add_new_list(self, name_of_list_to_add):
        self.wait.until(expected_conditions.visibility_of_element_located(BoardPage.name_of_new_list_field))
        new_list_field = self.driver.find_element(*BoardPage.name_of_new_list_field)
        new_list_field.clear()
        new_list_field.send_keys(name_of_list_to_add)
        save_new_list_button = self.driver.find_element(*BoardPage.save_new_list_button)
        save_new_list_button.click()

    def is_list_added(self, added_lists_name):
        set_of_new_lists = self.driver.find_elements(*BoardPage.name_of_added_list)
        names_of_lists_on_board = []
        for new_list in set_of_new_lists:
            names_of_lists_on_board.append(new_list.text)
        if added_lists_name in names_of_lists_on_board:
            return True
        else:
            return False

    def add_new_lists(self, list_of_names):
        if len(list_of_names) == 1:
            self.add_new_list(list_of_names[0])
        else:
            for name in list_of_names:
                self.add_new_list(name)

    def are_lists_added(self, list_of_names):
        set_of_new_lists = self.driver.find_elements(*BoardPage.name_of_added_list)
        names_of_lists_on_board = []
        for new_list in set_of_new_lists:
            names_of_lists_on_board.append(new_list.text)
        for new_list in list_of_names:
            if new_list not in names_of_lists_on_board:
                return False
        return True

    def copy_existing_list_and_change_name(self, name_of_existing_list, name_of_new_list):
        # find all names of existing lists
        set_of_new_lists = self.driver.find_elements(*BoardPage.name_of_added_list)
        # check list for name_of_existing_list
        for new_list in set_of_new_lists:
            if new_list.text == name_of_existing_list:
                list_header = new_list.find_element_by_xpath("..")
                menu_button = list_header.find_element(*BoardPage.list_menu_button)
                menu_button.click()
                self.driver.find_element(*BoardPage.list_menu_copy_list_option).click()
                new_list_name_field = self.driver.find_element(*BoardPage.list_menu_new_name_field)
                new_list_name_field.clear()
                new_list_name_field.send_keys(name_of_new_list)
                create_new_list_button = self.driver.find_element(*BoardPage.list_menu_copy_list_submit_button)
                create_new_list_button.click()

    def archive_list(self, name_of_list_to_be_archived):
        set_of_new_lists = self.driver.find_elements(*BoardPage.name_of_added_list)
        # check list for name_of_list_to_be_archived
        for new_list in set_of_new_lists:
            if new_list.text == name_of_list_to_be_archived:
                list_header = new_list.find_element(By.XPATH, "..")
                menu_button = list_header.find_element(*BoardPage.list_menu_button)
                menu_button.click()
                self.wait.until(
                    expected_conditions.visibility_of_element_located(BoardPage.list_menu_archive_list_option))
                self.wait.until(
                    expected_conditions.element_to_be_clickable(BoardPage.list_menu_archive_list_option))
                self.driver.find_element(*BoardPage.list_menu_archive_list_option).click()

    def is_list_archived(self, name_of_archived_list):
        set_of_new_lists = self.driver.find_elements(*BoardPage.name_of_added_list)
        set_of_lists_names = []
        for new_list in set_of_new_lists:
            set_of_lists_names.append(new_list.text)
        if name_of_archived_list in set_of_lists_names:
            return False
        return True

    def archive_all_lists(self):
        lists_menu_buttons = self.driver.find_elements(*BoardPage.list_menu_button)
        for i in range(0, len(lists_menu_buttons)):
            self.wait.until(expected_conditions.element_to_be_clickable(BoardPage.list_menu_button))
            self.driver.find_element(*BoardPage.list_menu_button).click()
            # self.wait.until(
            #     expected_conditions.element_to_be_clickable(BoardPage.list_menu_archive_list_option))
            sleep(1)
            self.driver.find_element(*BoardPage.list_menu_archive_list_option).click()
            self.wait.until(
                expected_conditions.invisibility_of_element_located(BoardPage.list_menu_archive_list_option))

    def are_all_lists_archived(self):
        set_of_new_lists = self.driver.find_elements(*BoardPage.name_of_added_list)
        if len(set_of_new_lists) == 0:
            return True
        else:
            return False

    def rename_list(self, list_to_be_renamed, new_name_of_list):
        set_of_new_lists = self.driver.find_elements(*BoardPage.name_of_added_list)
        for new_list in set_of_new_lists:
            if new_list.text == list_to_be_renamed:
                name_field = new_list.find_element(By.XPATH, "..")
                name_field.find_element(*BoardPage.change_name_field).click()
                name_field.find_element(*BoardPage.name_of_added_list).clear()
                name_field.find_element(*BoardPage.name_of_added_list).send_keys(new_name_of_list)
                name_field.find_element(*BoardPage.name_of_added_list).send_keys(Keys.ENTER)

    def is_list_renamed(self, list_to_be_renamed, new_name_of_list):
        set_of_new_lists = self.driver.find_elements(By.CLASS_NAME, "js-list-name-input")
        for new_list in set_of_new_lists:
            if new_list.text == list_to_be_renamed:
                name_field = new_list.find_element(By.XPATH, "..")
                title = name_field.find_element(By.CLASS_NAME, "list-header-name-assist")
                if title.get_property("innerText") == new_name_of_list:
                    return True
        return False
