from pages.base import BasePage
from pages.board import BoardPage
from pages.logout import LogoutPage
from tests.data import TestData
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions


class MainPage(BasePage):
    class BoardType:
        DEFAULT = 0
        PRIVATE = 1
        PUBLIC = 2

    address = "https://trello.com/"
    page_title = "Tablice | Trello"
    menu_icon = (By.CLASS_NAME, "member-initials")
    menu_logout_option = (By.CLASS_NAME, "js-logout")

    boards_link_button = (By.CSS_SELECTOR, "")
    main_page_title = "Strona główna | Trello"
    main_page_address = "https://trello.com/abrytfanna/boards"

    # create_new_board_button = (By.LINK_TEXT, "Utwórz nową tablicę…")
    create_board_form = (By.CLASS_NAME, "create-board-form")
    create_new_board_button = (By.CLASS_NAME, "mod-add")

    new_board_dialog_name_field = (By.CLASS_NAME, "subtle-input")
    new_board_dialog_type_button = (By.CLASS_NAME, "vis-chooser-trigger")
    new_board_dialog_private_board_type = (By.CSS_SELECTOR, "span.icon-sm.icon-private")
    new_board_dialog_public_board_type = (By.CSS_SELECTOR, "span.icon-sm.icon-public")
    new_board_dialog_create_button = (By.CLASS_NAME, "primary")
    new_board_dialog_close_dialog_button = (By.CLASS_NAME, "hide-dialog-trigger")

    boards_menu_button = (By.CLASS_NAME, "js-boards-menu")
    boards_menu_create_new_board_button = (By.CLASS_NAME, "js-add-board")

    create_board_or_team_menu_button = (By.CLASS_NAME, "js-open-add-menu")
    create_board_or_team_menu_new_board_button = (By.CLASS_NAME, "js-new-board")

    def go_to_list_of_boards(self):
        if self.driver.title == MainPage.main_page_title:
            self.driver.get(MainPage.main_page_address)
            self.wait.until(expected_conditions.title_is(MainPage.page_title))

    def logout(self):
        MainPage.click_menu_icon(self)
        logout_option_button = self.driver.find_element(*MainPage.menu_logout_option)
        logout_option_button.click()

    def click_menu_icon(self):
        icon_menu = self.driver.find_element(*MainPage.menu_icon)
        icon_menu.click()

    def is_logout_successful(self):
        self.wait.until(expected_conditions.title_is(LogoutPage.page_title))
        page_title = self.driver.title
        return page_title == LogoutPage.page_title

    def click_boards_button_left_menu(self):
        self.wait.until(expected_conditions.presence_of_element_located(MainPage.boards_menu_button))
        boards_button = self.driver.find_element(*MainPage.boards_menu_button)
        boards_button.click()
        self.wait.until(expected_conditions.presence_of_element_located(MainPage.boards_menu_create_new_board_button))

    def click_create_new_board_left_menu(self):
        create_board_button = self.driver.find_element(*MainPage.boards_menu_create_new_board_button)
        create_board_button.click()

    def click_create_board_or_team_button(self):
        self.driver.find_element(*MainPage.create_board_or_team_menu_button).click()

    def click_create_board_or_team_new_board_button(self):
        self.wait.until(expected_conditions.presence_of_element_located(MainPage.create_board_or_team_menu_new_board_button))
        self.driver.find_element(*MainPage.create_board_or_team_menu_new_board_button).click()

    def click_create_new_board_button(self):
        self.wait.until(expected_conditions.presence_of_element_located(MainPage.create_new_board_button))
        new_board_button = self.driver.find_element(*MainPage.create_new_board_button)
        new_board_button.click()

    def set_new_board_name(self, board_name):
        new_board_name = self.driver.find_element(*MainPage.new_board_dialog_name_field)
        new_board_name.click()
        new_board_name.clear()
        for letter in board_name:
            new_board_name.send_keys(letter)

    def set_new_board_type(self, board_type):
        board_type_button = self.driver.find_element(*MainPage.new_board_dialog_type_button)
        board_type_button.click()

        if board_type == MainPage.BoardType.PUBLIC:
            self.wait.until(expected_conditions.element_to_be_clickable(MainPage.new_board_dialog_public_board_type))
            public_board_type_button = self.driver.find_element(*MainPage.new_board_dialog_public_board_type)
            public_board_type_button.click()
        else:
            self.wait.until(expected_conditions.element_to_be_clickable(MainPage.new_board_dialog_private_board_type))
            private_board_type_button = self.driver.find_element(*MainPage.new_board_dialog_private_board_type)
            private_board_type_button.click()

    def click_create_new_board_button_dialog(self):
        create_board_button = self.driver.find_element(*MainPage.new_board_dialog_create_button)
        create_board_button.click()
        self.wait.until(expected_conditions.invisibility_of_element_located(MainPage.create_board_form))

    def create_new_board(self, board_name, board_type):
        self.click_create_new_board_button()
        self.set_new_board_name(board_name)
        if board_type != MainPage.BoardType.DEFAULT:
            self.set_new_board_type(board_type)
        self.click_create_new_board_button_dialog()

    def create_new_board_left_menu(self, board_name, board_type):
        self.click_boards_button_left_menu()
        self.click_create_new_board_left_menu()
        self.set_new_board_name(board_name)
        if board_type != MainPage.BoardType.DEFAULT:
            self.set_new_board_type(board_type)
        self.click_create_new_board_button_dialog()

    def create_new_board_from_board_or_team_menu(self, board_name, board_type):
        self.click_create_board_or_team_button()
        self.click_create_board_or_team_new_board_button()
        self.set_new_board_name(board_name)
        if board_type != MainPage.BoardType.DEFAULT:
            self.set_new_board_type(board_type)
        self.click_create_new_board_button_dialog()

    # def is_dialog_closed_correctly(self):
    #     try:
    #         self.driver.find_element(*MainPage.create_board_form)
    #         return False
    #     except:
    #         return True

    def is_board_created_correctly(self):
        page_title = self.driver.title
        page_url = self.driver.current_url
        return page_title == BoardPage.title and TestData.test_board_name in page_url
