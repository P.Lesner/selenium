from HtmlTestRunner import HTMLTestRunner
import unittest
from pages.board import BoardPage
from tests.BaseTests import BaseTestOnBoardPage


class ArchivingListTest(BaseTestOnBoardPage):

    def test_archiving_one_list(self):
        board_page = BoardPage(self.driver)
        lists_names = ["My list", "My second list"]
        board_page.add_new_lists(lists_names)
        board_page.archive_list("My second list")
        assert board_page.is_list_archived("My second list")
        assert not board_page.is_list_archived("My list")

    def test_archiving_all_existing_lists(self):
        board_page = BoardPage(self.driver)
        lists_names = ["My favourite list", "list", "To do", "My list 4"]
        board_page.add_new_lists(lists_names)
        board_page.archive_all_lists()
        assert board_page.are_all_lists_archived()


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))
