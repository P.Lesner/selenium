from HtmlTestRunner import HTMLTestRunner
import unittest
from pages.board import BoardPage
from tests.BaseTests import BaseTestOnBoardPage


class CreatingListOnTableTest(BaseTestOnBoardPage):

    def test_create_list_with_correct_name(self):
        board_page = BoardPage(self.driver)
        board_page.add_new_list("My to do list")
        assert board_page.is_list_added("My to do list")

    def test_create_few_lists_with_correct_name(self):
        board_page = BoardPage(self.driver)
        lists_names = ["My list A", "My second list B", "Important !!!", "4", ":)", "To do ?"]
        board_page.add_new_lists(lists_names)
        assert board_page.are_lists_added(lists_names)

    def test_create_list_with_whitespace_name(self):
        board_page = BoardPage(self.driver)
        board_page.add_new_list(" ")
        assert not board_page.is_list_added(" ")

    def test_copy_existing_list(self):
        board_page = BoardPage(self.driver)
        lists_names = ["My list", "Next list", "Final list"]
        board_page.add_new_lists(lists_names)
        board_page.copy_existing_list_and_change_name("Next list", "Next list 2A")
        assert board_page.is_list_added("Next list 2A")


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))
