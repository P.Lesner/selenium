from HtmlTestRunner import HTMLTestRunner
from selenium import webdriver
import unittest
from pages.login import LoginPage
from tests.user_data import User


class LoginTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get(LoginPage.address)

    def tearDown(self):
        self.driver.close()

    def test_login_correct_email_correct_password(self):
        login_page = LoginPage(self.driver)
        login_page.login(User.correct_email, User.correct_password)
        assert login_page.is_login_successful()

    def test_login_correct_login_correct_password(self):
        login_page = LoginPage(self.driver)
        login_page.login(User.correct_login, User.correct_password)
        assert login_page.is_login_successful()

    def test_login_incorrect_email_correct_password(self):
        login_page = LoginPage(self.driver)
        login_page.login(User.incorrect_email, User.correct_password)
        assert login_page.incorrect_email_error_displayed()

    def test_login_correct_email_incorrect_password(self):
        login_page = LoginPage(self.driver)
        login_page.login(User.correct_email, User.incorrect_password)
        assert login_page.incorrect_password_error_displayed()

    def test_login_correct_email_blank_password(self):
        login_page = LoginPage(self.driver)
        login_page.login(User.correct_email, User.blank_password)
        assert login_page.incorrect_password_error_displayed()


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))
