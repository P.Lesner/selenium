class TestData():
    # Board data
    test_private_board_name = "Private Test Board"
    test_public_board_name = "Public Test Board"
    test_board_rename = "Rename Test Board"
    test_copy_board_name = "Copy Test Board"
    test_board_name = "test-name"

    # List data
    correct_name_of_list = "test-name"
    empty_name_of_list = " "
