from unittest import TestLoader, TestSuite
from HtmlTestRunner import HTMLTestRunner
from tests.LoginTests import LoginTest
from tests.LogoutTests import LogoutTest
from tests.CreatingListTests import CreatingListOnTableTest
from tests.ArchivingListTests import ArchivingListTest
from tests.BoardTests import BoardTests
from tests.EditingListTests import EditingListOnTableTest

login_tests = TestLoader().loadTestsFromTestCase(LoginTest)
logout_tests = TestLoader().loadTestsFromTestCase(LogoutTest)
creating_list_tests = TestLoader().loadTestsFromTestCase(CreatingListOnTableTest)
archiving_list_tests = TestLoader().loadTestsFromTestCase(ArchivingListTest)
editing_list_tests = TestLoader().loadTestsFromTestCase(EditingListOnTableTest)
board_tests = TestLoader().loadTestsFromTestCase(BoardTests)

suite = TestSuite([login_tests, logout_tests, creating_list_tests, editing_list_tests, archiving_list_tests, board_tests])

runner = HTMLTestRunner(output='')

runner.run(suite)
